package com.kata.demo;

import com.kata.demo.entity.Account;
import com.kata.demo.entity.Operation;

import java.math.BigDecimal;

import static com.kata.demo.factory.OperationPrinter.printOperation;
import static com.kata.demo.type.OperationType.DEPOSIT;
import static com.kata.demo.type.OperationType.WITHDRAWAL;
import static java.time.LocalDate.now;

public class KataApplication {

    public static void main(String[] args) {

        String accountNumber = "FR001-007";
        Account account = new Account(accountNumber, BigDecimal.ZERO);

        Operation operationNo1 = new Operation(DEPOSIT, accountNumber, BigDecimal.valueOf(10000), BigDecimal.valueOf(10000), now() );
        account.deposit(BigDecimal.valueOf(10000));
        account.addOperation(operationNo1);

        Operation operationNo2 = new Operation(WITHDRAWAL, accountNumber,BigDecimal.valueOf(2000), BigDecimal.valueOf(8000), now());
        account.withdrawal(BigDecimal.valueOf(2000));
        account.addOperation(operationNo2);

        Operation operationNo3 = new Operation(WITHDRAWAL, accountNumber,BigDecimal.valueOf(1000), BigDecimal.valueOf(7000), now());
        account.withdrawal(BigDecimal.valueOf(1000));
        account.addOperation(operationNo3);

        printOperation(account.getOperations());
    }
}
