package com.kata.demo.constant;

public class Constant {

    public static final String HEADER = "OPERATION" +"\t" + "\t" + "DATE" + "\t" + "\t" +  "\t" + "AMOUNT" + "\t" + "\t" + "BALANCE";
    public static final String LINE_SEPARATOR = "----------------------------------------------------------";
    public static final String HEADER_FORMAT = "%37s";
    public static final String LINES_FORMAT = "%36s";
}
