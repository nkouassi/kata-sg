package com.kata.demo.exception;

public class UnsufficientBalanceException extends RuntimeException{

    /**
     * Constructor with parameter.
     *
     * @param message The message to be printed when exception is thrown.
     */
    public UnsufficientBalanceException(String message){
        super(message);
    }
}
