package com.kata.demo.factory;

import com.kata.demo.entity.Operation;

import java.util.List;

import static com.kata.demo.constant.Constant.*;

/**
 * Class for the management of the operations printing.
 */
public class OperationPrinter {

    /**
     * Print the list of the performed operations.
     *
     * @param operations the operations to be printed.
     */
    public static void printOperation(List<Operation> operations){

    StringBuilder str = new StringBuilder(LINE_SEPARATOR);
    str.append("\n");
    str.append(String.format(HEADER_FORMAT, HEADER));
    str.append("\n");
    str.append(LINE_SEPARATOR);
    str.append("\n");
    operations.stream()
            .forEach(operation -> {
                str.append(String.format(LINES_FORMAT, operation.toString()));
                str.append("\n");
            });
    str.append(LINE_SEPARATOR);
    System.out.print(str);
    }
}
