package com.kata.demo.entity;

import com.kata.demo.type.OperationType;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.kata.demo.type.OperationType.DEPOSIT;

/**
 * View for the operations management.
 */
public record Operation(OperationType operationType, String accountNumber, BigDecimal amount, BigDecimal balance, LocalDate createdDate) {

    @Override
    public String toString(){

        StringBuilder str = new StringBuilder();
        str.append(operationType);
        if(operationType == DEPOSIT){
            str.append("\t" + "\t" + "\t");
        }else {
            str.append("\t" + "\t");
        }
        str.append(createdDate);
        str.append("\t" + "\t");
        str.append(amount);
        str.append("\t" + "\t");
        str.append(balance);

        return str.toString();
    }
}