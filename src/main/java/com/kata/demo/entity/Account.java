package com.kata.demo.entity;

import com.kata.demo.exception.UnsufficientBalanceException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity for the account management.
 */
public class Account {

    private String accountNumber;

    private BigDecimal balance;

    private List<Operation> operations = new ArrayList<>();

    public Account(){
    }

    /**
     * Constructor with parameters.
     *
     * @param accountNumber the reference number of the customer account.
     * @param balance the available balance.
     */
    public Account(String accountNumber, BigDecimal balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    /**
     * Make a deposit on the customer account.
     *
     * @param amount The deposit amount.
     */
    public void deposit(BigDecimal amount){
        checkOperationAmount(amount);
        setBalance(this.balance.add(amount));
    }

    /**
     * Make a withdrawal on the customer account.
     *
     * @param amount The withdrawal amount.
     */
    public void withdrawal(BigDecimal amount){

        checkOperationAmount(amount);
        if(this.balance.compareTo(amount) < 0){
            throw new UnsufficientBalanceException("Your balance is unsufficient to perform this operation");
        }
        setBalance(this.balance.subtract(amount));
    }

    /**
     * Add the operations linked to the account.
     *
     * @param operation The operation to be added.
     */
    public void addOperation(Operation operation){
        operations.add(operation);
    }

    /**
     * Check the validity of the operation's amount.
     *
     * @param amount The amount to be checked.
     */
    private void checkOperationAmount(BigDecimal amount){

        if(amount == null){
            throw new IllegalArgumentException("The amount should not be null");
        }

        if(amount.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("The amount should be a positive value");
        }
    }

}
