package com.kata.demo.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;

import static com.kata.demo.constant.Constant.*;
import static com.kata.demo.factory.OperationPrinter.printOperation;
import static com.kata.demo.type.OperationType.DEPOSIT;
import static com.kata.demo.type.OperationType.WITHDRAWAL;
import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

class OperationTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void reinitialize() {
        System.setOut(originalOut);
    }

    @Test
    void Should_PrintOperationHistory_WhenOperationsAreMade() {

        // GIVEN
        String accountNumber = "CI-005-009";

        Operation operation1 = new Operation(DEPOSIT, accountNumber, BigDecimal.valueOf(10000), BigDecimal.valueOf(10000), now());
        Operation operation2 = new Operation(WITHDRAWAL, accountNumber, BigDecimal.valueOf(3000), BigDecimal.valueOf(7000), now());

        StringBuilder str = new StringBuilder(LINE_SEPARATOR);
        str.append("\n");
        str.append(String.format(HEADER_FORMAT, HEADER));
        str.append("\n");
        str.append(LINE_SEPARATOR);
        str.append("\n");
        str.append(String.format(LINES_FORMAT, operation1));
        str.append("\n");
        str.append(String.format(LINES_FORMAT, operation2));
        str.append("\n");
        str.append(LINE_SEPARATOR);

        // WHEN
        printOperation(asList(operation1, operation2));

        // THEN
        assertThat(str.toString()).isEqualTo(outContent.toString());
    }
}
