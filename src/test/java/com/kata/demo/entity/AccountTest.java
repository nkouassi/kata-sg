package com.kata.demo.entity;

import com.kata.demo.exception.UnsufficientBalanceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.kata.demo.type.OperationType.DEPOSIT;
import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;

class AccountTest {

    private Account account;
    private String accountNumber;
    private BigDecimal operationAmount;

    @BeforeEach
    void setUp(){
        accountNumber = "CI-001-007";
    }

    @Test
    void Should_MakeDepositOnAccount_WhenAmountIsValid() {

        // GIVEN
        operationAmount = BigDecimal.valueOf(10000);
        account = new Account(accountNumber, BigDecimal.ZERO);

        // WHEN
        account.deposit(operationAmount);

        // THEN
        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(10000));
    }

    @Test
     void Should_ThrowIllegalArgumentException_WhenAmountIsNull() {

        // GIVEN
        account = new Account(accountNumber, BigDecimal.ZERO);

        // WHEN
        Throwable thrown = catchThrowable(() -> {
            account.deposit(operationAmount);
        });

        // THEN
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("The amount should not be null");
    }

    @Test
    void Should_ThrowIllegalArgumentException_WhenAmountIsNegative() {

        // GIVEN
        operationAmount = BigDecimal.valueOf(-10000);
        account = new Account(accountNumber, BigDecimal.valueOf(10000));

        // WHEN
        Throwable thrown = catchThrowable(() -> {
            account.deposit(operationAmount);
        });

        // THEN
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("The amount should be a positive value");
    }

    @Test
    void Should_AddOperationToOperationsList_WhenOperationIsMade(){

        // GIVEN
        operationAmount = BigDecimal.valueOf(50000);
        BigDecimal balance = BigDecimal.valueOf(10000);
        account = new Account(accountNumber, BigDecimal.ZERO);
        Operation operation = new Operation(DEPOSIT, accountNumber, operationAmount, balance, now());

        // WHEN
        account.addOperation(operation);

        // THEN
        assertThat(account.getOperations()).hasSize(1);
        assertThat(account.getOperations()).containsOnly(operation);
    }

    @Test
    void Should_MakeWithdrawalOnAccount_WhenAmountAndBalanceAreValid() {

        // GIVEN
        operationAmount = BigDecimal.valueOf(1500);
        account = new Account(accountNumber, BigDecimal.valueOf(10000));

        // WHEN
        account.withdrawal(operationAmount);

        // THEN
        assertThat(account.getBalance()).isEqualTo(BigDecimal.valueOf(8500));
    }

    @Test
    void Should_ThrowArithmeticException_WhenBalanceIsNotSufficient() {

        // GIVEN
        operationAmount = BigDecimal.valueOf(10000);
        account = new Account(accountNumber, BigDecimal.valueOf(5000));

        // WHEN
        Throwable thrown = catchThrowable(() -> {
            account.withdrawal(operationAmount);
        });

        // THEN
        assertThat(thrown)
                .isInstanceOf(UnsufficientBalanceException.class)
                .hasMessage("Your balance is unsufficient to perform this operation");
    }

}
