###  = KATA ==(no frameworks are required, just focus on solving the domain problem)

### Bank account kata Think of your personal bank account experience When in doubt, go for the simplest solution.

### Requirements
·	Deposit and Withdrawal
·	Account statement (date, amount, balance)
·	Statement printing

### User Stories

·   US 1:
In order to save money
As a bank client
I want to make a deposit in my account

·   US 2:
In order to retrieve some or all of my savings
As a bank client
I want to make a withdrawal from my account

·   US 3:
In order to check my operations
As a bank client
I want to see the history (operation, date, amount, balance) of my operations

### Trick for activate preview features
If it's not done yet, you can activate preview features by add the following lines into the build section of your pom.xml file. <br/>
Source and target represent the java version which is used

# <build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>18</source>
                    <target>18</target>
                <compilerArgs>--enable-preview</compilerArgs>
            </configuration>
        </plugin>
    </plugins>
# </build>